FROM node:11.6.0-alpine AS builder
COPY . ./register-web-ui

WORKDIR /register-web-ui

RUN npm i
RUN $(npm bin)/ng build --prod

FROM nginx:1.15.8-alpine
COPY --from=builder /register-web-ui/dist/register-web-ui/ /usr/share/nginx/html